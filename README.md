# Description
`mystow.py` is a simple Python script to stow your dotfiles.

- It creates symlinks of files and directories from:

> ~/dotfiles/.config/

to:

> ~/.config/


and from:

> ~/dotfiles/

to:

> ~/


- It removes old links and relinks the files/dirs.
- If file/dir exists already, it copies it to a backup dir. It then links the file/dir from the source directory (ignoring the one in the backup dir).
- It checks for and deletes broken links in target directories (using option `--check-links`).

# Profiling

- Profiling is enabled by default, meaning that `mystow.py` can detect which symlinks are to be created, and which to ignore. This is signalled by means of a naming convention using specific suffixes, as explained below.

- For this to happen, `mystow.py` requires that the available profile names be included in a config file `mystow.yaml`, located on the local folder where `mystow.py` is placed, or, if not found there, in `~/.config`.

- `mystow.yaml` must adhere to the following syntax:


## Example of `mystow.yaml`

```
# mystow config
---
- profile: 1
  name: hp-laptop
- profile: 2
  name: dell-server
- profile: 3
  name: thinkpad-desktop
```

- If no config file is found, profiling is turned off, meaning that all symlinks will be created.

- `mystow.py` determines the current profile by reading the current hostname (with `socket.gethostname()`).

- Files/directories profiles are given by specific suffixes starting with a `_`.

- For instance:

```
...
emacs.d
emacs.d_hp-laptop
emacs.d_dell-server
emacs.d_thinkpad-desktop
...
```

- `mystow.py` then chooses only the file/directory that matches the current profile, and proceeds to create the symlink. It ignores all other profiled variations.

- For instance, if the current profile (the hostname) is `thinkpad-desktop`, then only the directory `emacs.d_thinkpad-desktop` gets symlinked. It is symlinked to `emacs.d`.


# Requires
- Python 3.6+
- pyyaml (`pip3 install pyyaml`)
- colorama (`pip3 install colorama`) (optional for color output)

# Installation
- Make sure you have Python 3.6+, the pyyaml module, and (optionally) the colorama module installed.
- Clone this repo: `git clone https://github.com/nivaca/mystow`
- Run the script `./mystow.py`

# Usage
```
mystow.py v.1.3 (ɔ) Nicolas Vaughan 2018
Usage: mystow.py
--config, only run on ~/.config
--home, only run on ~
--all, run on both ~ and ~/.config
--dry, to make a dry run (no changes are made)
--backup, to backup previous symlinks (default on)
--no-profiling, to disable software profiles
--check-links, deletes broken symlinks in ALL destination directories
--source=[source_dir], to optionally select another source directory where the dotfiles are located (default is ~/dotfiles)
--target=[target_dir], to optionally select another target directory (default is ~)
--help, to display this message
```

# License
[GPL3](http://www.gnu.org/licenses/gpl.html)
