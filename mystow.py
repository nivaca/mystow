#!/usr/bin/env python3

import os
import datetime
import shutil
import sys
import getopt
import re
import socket  # get the hostname

# yaml module --------------------------------
try:
    import yaml
except ImportError:
    print('Error: yaml module not available.\nPlease install pyyaml before proceeding.')
    sys.exit(0)

# colorama module --------------------------------
try:
    from colorama import init, Fore, Back, Style
except ImportError:
    print('Warning: colorama module not available')


    class AnsiCodes(object):
        def __init__(self):
            # the subclasses declare class attributes which are numbers.
            # Upon instantiation we define instance attributes, which are the same
            # as the class attributes but wrapped with the ANSI escape sequence
            for name in dir(self):
                if not name.startswith('_'):
                    value = getattr(self, name)

    class AnsiFore(AnsiCodes):
        BLACK = ''
        RED = ''
        GREEN = ''
        YELLOW = ''
        BLUE = ''
        MAGENTA = ''
        CYAN = ''
        WHITE = ''
        RESET = ''

    Fore = AnsiFore()

    colorama_exists = False
else:
    colorama_exists = True


# Globals ------------------------------------------
version = '1.3'
dryrun = False
make_backup = True
skip_dirs = [".config",
             ".local",
             "etc",
             "other",
             "root", ]  # do not stow this dirs
current_profile = socket.gethostname()
profiling_enabled = True
profiles = []
default_conf_file = "mystow.yaml"
run_in_config = False
run_in_home = False

home_dir = os.path.expanduser("~")
# home_dir = "/home/nivaca/dev/Python/mystow/temproot"  # testing only
config_dir = os.path.join(home_dir, ".config")
dotfiles_dir = os.path.join(home_dir, "dotfiles")
dotfiles_config_dir = os.path.join(dotfiles_dir, ".config")


# -----------------------------------------------------------------
def which_conf_file():
    global default_conf_file
    global profiling_enabled
    if os.path.isfile(default_conf_file):
        return default_conf_file
    if os.getenv('XDG_CONFIG_HOME', "") != "":
        xdg_config_home = os.path.join(
            os.path.expanduser("~"), ".config")
        conf_file = os.path.join(xdg_config_home, default_conf_file)
        if not os.path.isfile(conf_file):
            print(f"{default_conf_file} not found. Not using profiles")
            profiling_enabled = False
            return
        else:
            return conf_file


# -----------------------------------------------------------------
def read_conf_file(conf_file):
    """ Tries to read mystow.yaml. Returns a profile list """
    profiles = []

    try:
        with open(conf_file) as ymlfile:
            cfg = yaml.load(ymlfile)
    except OSError as err:
        print(f"*** OS error: {err} ***")
        sys.exit(1)
    for i in cfg:
        for k, v in i.items():
            if k == "profile":
                profiles.append(i["name"])
    return profiles

# -----------------------------------------------------------------


def compress_home_dir(path):
    """ Compresses the expanded home path to ~ """
    real_home = os.path.expanduser("~")
    return path.replace(real_home, "~")


# -----------------------------------------------------------------
def check_dirs_exist(dir_list):
    """ Checks if directories in the list exist """
    ok = True
    for d in dir_list:
        if not os.path.exists(d):
            print(f"{Fore.RED}{d} does not exist!")
            ok = False
    if not ok:
        return False
    else:
        return True


# -----------------------------------------------------------------
def is_broken(filename):
    """ Checks if filename is broken link
        https://stackoverflow.com/a/31102280
    """
    return os.path.islink(filename) and not os.path.exists(filename)


# -----------------------------------------------------------------
def safely_delete_link(filename):
    global dryrun
    if not dryrun:
        try:
            os.remove(filename)
        except OSError as err:
            print(f"{Fore.RED}*** OS error: {err} ***")
            sys.exit()


# -----------------------------------------------------------------
def delete_broken_kinks(target_path):
    """ finds and deletes broken links in path """
    filelist = os.listdir(target_path)
    count = 0
    for file in filelist:
        filename = os.path.join(target_path, file)
        if is_broken(filename):
            print(
                f"{Fore.RED}{compress_home_dir(filename)} is a broken link. Deleting...{Fore.RESET}")
            safely_delete_link(filename)
            count += 1
    return count


# -----------------------------------------------------------------
def check_links():
    global home_dir
    global config_dir
    print(
        f"Checking for dead links in {compress_home_dir(home_dir)} and {compress_home_dir(config_dir)}...")
    ccount = delete_broken_kinks(config_dir)
    if ccount > 1:
        print(f"{ccount} dead link(s) removed in {compress_home_dir(config_dir)}")
    hcount = delete_broken_kinks(home_dir)
    if hcount > 1:
        print(f"{hcount} dead link(s) removed in {compress_home_dir(home_dir)}")
    if (ccount + hcount) == 0:
        print(f"No dead links found")


# -----------------------------------------------------------------
def is_profiled(filename):
    """ Checks if file belongs to any one of the profiles. """
    answer = False
    for pattern in profiles:
        pattern = '.*_' + pattern + '$'
        if re.match(pattern, filename):
            answer = True
    return answer


# -----------------------------------------------------------------
def which_profile(filename):
    """ Returns which profile the filename belongs to """
    for pattern in profiles:
        pattern = '.*_' + pattern + '$'
        if re.match(pattern, filename):
            return pattern[3:-1]


# -----------------------------------------------------------------
def under_current_profile(filename):
    """ Checks if the filename belongs to the current profile """
    pattern = '.*_' + current_profile + '$'
    if re.match(pattern, filename):
        return True
    else:
        return False


# -----------------------------------------------------------------
def remove_suffix(filename):
    """ Removes any valid suffix from the filename """
    pat = '_' + which_profile(filename)
    return re.sub(pat, "", filename)


# -----------------------------------------------------------------
def is_base_file(filename, filelist):
    for profile in profiles:
        fullfilename = filename + '_' + profile
        if fullfilename in filelist:
            return True
    return False


# -----------------------------------------------------------------
def remove_unneeded_files(filelist):
    templist = []
    for filename in filelist:
        if is_profiled(filename):
            if under_current_profile(filename):
                templist.append(filename)
        else:
            if not is_base_file(filename, filelist):
                templist.append(filename)
    return templist


# -----------------------------------------------------------------
def clean_file_list(filelist):
    # First, exclude the files/dirs in the skip list
    templist = []
    for filename in filelist:
        if filename in skip_dirs:
            continue
        templist.append(filename)

    if profiling_enabled:
        templist = remove_unneeded_files(templist)

    if len(templist) == 0:
        print(f"{Fore.YELLOW}Warning: no files in source directory!{Fore.RESET}")
        sys.exit(2)
    else:
        return templist


# -----------------------------------------------------------------
def create_bak_dir(target):
    """ creates a bakdir
        Returns: name of bakdir; or an empty string,
        in case no backing up or dry run is selected
    """
    global dryrun
    global make_backup
    bakname = "bak-" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    bakdir = os.path.join(target, bakname)
    if (not dryrun) and make_backup:
        try:
            os.makedirs(bakdir, exist_ok=False)
        except OSError as err:
            print(f"{Fore.RED}*** OS error: {err} ***")
            sys.exit()
        return bakdir
    else:
        return ""


# -----------------------------------------------------------------
def delete_item(target):
    """ Deletes a file or directory """
    global dryrun
    if not dryrun:
        if os.path.isdir(target):
            try:
                shutil.rmtree(target)
            except OSError as err:
                print(f"{Fore.RED}*** OS error: {err} ***")
                sys.exit()
        if os.path.isfile(target):
            try:
                os.remove(target)
            except OSError as err:
                print(f"{Fore.RED}*** OS error: {err} ***")
                sys.exit()


# -----------------------------------------------------------------
def delete_dir(target):
    """ Safely delete directory if EMPTY"""
    global dryrun
    if not dryrun:
        try:
            shutil.rmtree(target)
        except OSError as err:
            print(f"{Fore.RED}*** OS error: {err} ***")
            sys.exit()


# -----------------------------------------------------------------
def safely_move_item(source, target):
    """ Safely move an item """
    if not dryrun:
        try:
            shutil.move(source, target)
        except OSError as err:
            print(f"{Fore.RED}*** OS error: {err} ***")
            sys.exit()
    if not dryrun:
        print(
            f"Moving {compress_home_dir(source)} into {compress_home_dir(target)}")


# -----------------------------------------------------------------
def create_symlink(source, target):
    """ Safely create a symlink """
    if not dryrun:
        try:
            os.symlink(source, target)
        except OSError as err:
            print(f"{Fore.RED}*** OS error: {err} ***")
            sys.exit()


# -----------------------------------------------------------------
def stow(source_dir, target_dir):
    """ Stows all files/dirs from source_dir to target_dir """

    bakdir = create_bak_dir(target_dir)
    filelist = clean_file_list(os.listdir(source_dir))

    for filename in filelist:
        # default option in case filename
        # does not belong to any profile
        outfile = filename

        if profiling_enabled:
            if is_profiled(filename):
                if under_current_profile(filename):
                    outfile = remove_suffix(filename)
                else:
                    continue
        fullsrc = os.path.join(source_dir, filename)
        fulldest = os.path.join(target_dir, outfile)

        # If item already exists, delete it
        if os.path.exists(fulldest):
            if make_backup:
                safely_move_item(fulldest, os.path.join(bakdir, filename))
                # print(f"{compress_home_dir(fulldest)} exists.")
                if not dryrun:
                    print(f"Backing up in {compress_home_dir(bakdir)}...")
            else:
                delete_item(fulldest)
                print(f"{compress_home_dir(fulldest)} exists; deleting...")

        # Create the symlink
        create_symlink(fullsrc, fulldest)
        print(f"{Fore.GREEN}Creating symlink {compress_home_dir(fullsrc)}{Fore.RESET} {Fore.RED}->{Fore.GREEN} {compress_home_dir(fulldest)}{Fore.RESET}")

    # Delete bakdir only if empty
    if (not dryrun) and make_backup:
        if len(os.listdir(bakdir)) == 0:
            delete_dir(bakdir)


# -----------------------------------------------------------------
def usage():
    """ Prints the script's parameters """
    conf_file = which_conf_file()
    if conf_file is None:
        pass
    elif conf_file == default_conf_file:
        print(f"{default_conf_file} file found in local folder")
    else:
        print(f"{default_conf_file} file found in {compress_home_dir(conf_file)}")

    print(f"mystow.py v.{version}. (ɔ) Nicolas Vaughan 2018", f"""
Usage: mystow.py
--config, only run on ~/.config
--home, only run on ~
--all, run on both ~ and ~/.config
--dry, to make a dry run (no changes are made)
--backup, to backup previous symlinks {Fore.BLUE}(default on){Fore.RESET}
--no-profiling, to disable software profiles
--check-links, deletes broken symlinks in ALL destination directories
--source=[source_dir], to optionally select another source directory where the dotfiles are located {Fore.BLUE}(default is ~/dotfiles){Fore.RESET}
--target=[target_dir], to optionally select another target directory {Fore.BLUE}(default is ~){Fore.RESET}
--help, to display this message
""")


# -------------------------------------------------------------------
def main():
    """ Main function. """
    global dryrun
    global make_backup
    global profiling_enabled
    global profiles
    global current_profile
    global run_in_home
    global run_in_config
    global home_dir
    global config_dir
    global dotfiles_config_dir
    global dotfiles_dir

    validate_files = False

    if colorama_exists:
        init()

    argv = sys.argv[1:]

    if argv == []:
        usage()
        sys.exit(2)
    else:
        if argv[0] == '':
            usage()
            sys.exit(2)

    try:
        opts, args = getopt.getopt(argv, '', [
            'config', 'home', 'all', 'dry', 'help', 'backup',
            'check-links', 'no-profiling', 'source=', 'destination=', 'profile='])
    except getopt.GetoptError:
        print(f"{Fore.RED}Command argument error.{Fore.RESET}")
        usage()
        sys.exit(2)

    if len(sys.argv) == 1:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == "--help":
            usage()
            sys.exit(0)
        if opt == "--dry":
            dryrun = True
        if opt == "--home":
            run_in_home = True
        if opt == "--config":
            run_in_config = True
        if opt == "--no-profiling":
            profiling_enabled = False
        if opt == "--backup":
            make_backup = True
        if opt == "--check-links":
            validate_files = True
        if opt == "--source":
            dotfiles_dir = arg
        if opt == "--destination":
            home_dir = arg
        if opt == "--profile":
            current_profile = arg
        if opt == "--all":
            run_in_config = True
            run_in_home = True

    if validate_files:
        check_links()
        sys.exit()

    if profiling_enabled:
        conf_file = which_conf_file()
        if conf_file is None:
            print(f"{Fore.RED}Error: Profiling enabled but no config file found! Aborting...")
            sys.exit(2)
        else:
            profiles = read_conf_file(conf_file)

    if dryrun:
        print(f"{Fore.BLUE}*** Dry running (no changes being made)...")

    if not check_dirs_exist([home_dir, dotfiles_dir, config_dir, dotfiles_config_dir]):
        print(f"{Fore.RED}Aborting...")
        sys.exit(1)

    if run_in_config:
        print(
            f"{Fore.MAGENTA}*** Running on {compress_home_dir(config_dir)}:{Fore.RESET}")
        stow(dotfiles_config_dir, config_dir)

    if run_in_home:
        print(
            f"{Fore.MAGENTA}*** Running on {compress_home_dir(home_dir)}:{Fore.RESET}")
        stow(dotfiles_dir, home_dir)


# -----------------------------------------------------------------
if __name__ == '__main__':
    main()
